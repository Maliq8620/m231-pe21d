# Ablagekonzept <!-- omit in toc -->

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Ziele](#1-ziele)
- [2. Applikationen und Daten trennen ... Wieso?](#2-applikationen-und-daten-trennen--wieso)
- [3. IST-Zustand: Wo speichere ich welche Daten ab?](#3-ist-zustand-wo-speichere-ich-welche-daten-ab)
  - [3.1. Vorgehen](#31-vorgehen)
    - [3.1.1. Vorlage - Spalten für die Tabelle Apps/Services](#311-vorlage---spalten-für-die-tabelle-appsservices)
- [4. Das eigene Ablagekonzept grafisch darstellen](#4-das-eigene-ablagekonzept-grafisch-darstellen)
  - [4.1. Vorgehen](#41-vorgehen)
  - [4.2. Beispiel](#42-beispiel)
- [5. Das eigene Ablagekonzept analysieren](#5-das-eigene-ablagekonzept-analysieren)
- [6. Weiterführende Links und Quellen](#6-weiterführende-links-und-quellen)

# 1. Ziele
 - Kann zwischen allgemeinen und personenbezogenen Daten unterscheiden.
 - B1G: Kann den Grundsatz Daten und Applikationen zu trennen erläutern
 - B1F: Wendet ein vorgegebenes Ablagekonzept an, um die eigenen Daten zu organisieren und zu speichern.
 - B1E: Entwirft ein eigenes Ablagekonzept für seine Daten und setzt dieses um.
 - B2G: Kann den Unterschied zwischen lokaler und cloudbasierter Ablage erläutern.
 - B2F: Richtet und setzt lokale und cloudbasierte Ablagen ein.
 - B2E: Vergleicht verschiedene cloudbasierte Ablagen.


# 2. Applikationen und Daten trennen ... Wieso?
Stellen Sie sich vor, Sie müssten jedes Mal, wenn Sie ihre Wohnung betreten, gleich nach dem Öffnen der Wohnungstür den Wasserhahn im Bad öffnen und für 3 Minuten laufen lassen, weil sonst am nächsten Morgen der Supermarkt um die Ecke sein Rolltor nicht öffnen kann. Das Beispiel scheint völlig absurd, zudem gibt es überhaupt keinen ersichtlichen Zusammenhang zwischen den beiden Dingen. Gefriert sonst das Wasser und das Rolltor wird mit einer Wasserpumpe geöffnet? Aber was ist dann im Sommer? Und weshalb würde man so etwas bauen? Ist das Absicht? 
Lassen wir uns auf dieses absurde Beispiel ein und betrachten es unter den folgenden Gesichtspunkten:
 - **Interessen:** Es gibt direktes Interesse für den/die Bewohner(in) den Wasserhahn zu öffnen. Ein übergeordnetes Interesse (z.B. weil er sonst nicht einkaufen kann/darf oder weil der Ladenbesitzer mit Gewalt droht) müsste vorliegen, dass der/die Bewohner(in) den Wasserhahn pflichtbewusst bei jedem Betreten seiner Wohnung öffnet. 
 - **Abhängigkeit:** Zwischen dem Tor und dem Wasserhahn besteht eine einseitige Abhängigkeit. Die Hauptfunktion des Rolltores noch des Wasserhahnes lassen nicht auf einen solchen Zusammenhang schliessen. 
 - **Nicht intuitiv:** Besucht jemand anders die Wohnung so muss dieser wissen, dass er gleich beim Betreten den Wasserhahn aufdrehen muss. Wenn das Aufdrehen des Wasserhahns vergessen oder unterlassen wird, dann gibt es kein unmittelbares Signal. Und selbst es halb acht morgens ist und sich der Ladenbesitzer aufregt, weil der Anwohner schon wieder vergessen hat seinen Wasserhahn zu öffnen, und wütend zur dessen Wohnung marschiert, um den Anwohner zurechtzuweisen: Stellen Sie sich vor, Sie sind bei Ihrem besten Freund(in) zu Gast und jemand taucht an der Wohnungstüre auf und schnauzt Sie an, Sie sollen gefälligst ihren Wasserhahn öffnen. Vermutlich würden nicht wenige davon ausgehen, dass der Ladenbesitzer komplett irre ist. 

Man würde denken, dass solche Situationen nie vorkommen. Doch findet man in der Informatik zahlreiche solche Beispiele. Meist werden aus Bequemlichkeit (Faulheit) unnötige und gefährliche Abhängigkeiten geschaffen, die für Dritte nur schwer nachvollziehbar sind. Häufig bleiben solche Abhängigkeiten unbemerkt, bis eines Tages eine Komponente ausfällt und es zu unerklärlichen Aktionen oder Systemabstürzen kommt. 

In der Informatik findet man deshalb den Grundsatz [*low coupling, high cohesio*](https://medium.com/clarityhub/low-coupling-high-cohesion-3610e35ac4a6):
 - Module haben nur so viele Abhängigkeiten haben wie unbedingt notwendig sind.
 - "We want to design components that are self-contained: independent, and with a single, well-defined purpose"
 - "When you come across a problem, assess how localized the fix is. Do you change just one module, or are the changes scattered throughout the entire system? When you make a change, does it fix everything, or do other problems mysteriously arise?"

Wenn wir Applikationen und Daten trennen, indem wir unsere Dokumente unter ```C:\Users\meinname\Documents``` ablegen und die passenden Programme unter ```C:\Program Files\```, verfolgen wir die Ziele: wenig Kopplung und gute Wartbarkeit:
 - Bei einer Neuinstallation einer Software muss nicht darauf geachtet werden, dass aus Versehen Daten des Benutzers gelöscht werden. 
 - Daten können unabhängig von der Applikationen gesichert werden. 
 - Bessere Übersichtlichkeit: Benutzerdaten und Applikationsdaten sind eindeutig voneinander zu halten. 


# 3. IST-Zustand: Wo speichere ich welche Daten ab?
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  2 Lektion |
| Ziele | Wo habe ich überall persönliche Daten gespeichert: Sich selber bewusst machen,  an welchen Speicherorten Daten über einen gespeichert sind. Methode: Auflistung von allen Geräten, Services und darauf abgespeicherten Daten. |

Jeder von uns verwendet verschiedene Geräte (Notebooks, Smartphone) und hat darauf persönliche Daten abgespeichert. In dieser Übung halten Sie fest auf welchem Gerät, Sie welche Informationen gespeichert haben und welche Services Sie verwenden. 
Lassen Sie bei Ihrer Auflistung die Daten von Ihrem Arbeitsort weg. 

**Tipp**: Lesen Sie zuerst die nachfolgende Aufträge. Eventuell fällt es Ihnen einfacher mit der grafischen Darstellung zu beginnen. 

## 3.1. Vorgehen
 1. Erstellen Sie eine Tabelle mit all ihren persönlichen Mikrocomputern (Notebook, Smartphone, usw.) mit den wichtigsten Spezifikationen (Individueller Name, Marke, Typ, Speichergrösse, usw...) und deren Verwendungszweck. 
 2. Erstellen Sie eine Tabelle mit allen Apps/Services, die mindestens eine der folgenden Kriterien erfüllen: <br> (A) Dient zur Kommunikation oder Kollaboration<br>(B) Beinhaltet personenbezogene Daten, die Sie selbst freigegeben / eingefügt haben. <br>Notieren Sie zu jedem App/Service, welche persönliche Daten diese beinhalten. 
 3. Erstellen Sie eine Tabelle mit allen Speichermedien auf denen Sie Daten abspeichern. (z.B. den Ordner DCIM mit Fotos auf Ihrem Smartphone). 

Halten Sie alles in Ihrem persönlichen Portfolio fest. 

![Beispiel Screenshot Tabelle](images/mydatatable.PNG)
*So könnte der Anfang Ihrer Tabelle aussehen.*


**Tipp:** Lassen Sie Apps, die nur allgemeine Personenbezogene Daten beinhalten (z.B. Name + Adresse für Lieferdienste) weg oder fassen Sie diese zusammen. 

### 3.1.1. Vorlage - Spalten für die Tabelle Apps/Services
 - Gerät (Speicherort)
 - Betriebssystem
 - Applikation
 - Art der Daten
 - Auf zentralem Server gespeichert?
 - User friendly privacy policy?
 - Folgen von Datenverlust (/Zugriffsverlust)<br/><i>Wäre es schlimm/Was wären die Konsequenzen, wenn die Daten unwiederruflich verlohren gehen?</i>
 - Folgen von Datendiebstahl<br/><i>Wäre es schlimm, wenn Freunde oder Fremde zugriff auf diese Daten hätten?</i>
 - Backup vorhanden?<br><i>Ja/Nein</i>
 - Art des Backups
 - Häufigkeit des Backups
 - Massnahmen<br/><i>Welche Massnahmen müssen ergriffen werden, damit die Anforderungen an Datenverlust/diebstahl erfüllt werden?</i>


# 4. Das eigene Ablagekonzept grafisch darstellen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  4 Lektion |
| Ziele | Übersichtsgrafik des persönliches Ablagekonzept erstellt. |

Mithilfe der Informationen aus der vorhergehenden Übung können Sie nun Ihr Ablagekonzept entwickeln. 

## 4.1. Vorgehen
 1. Wählen Sie ein vektorbasiertes Zeichnungsprogramm ([draw.io](draw.io), Microsoft Visio, o.ä.) aus.
 2. Stellen Sie nun die Daten aus der vorherigen Übung grafisch dar.

**Lassen Sie allgemeine Services (Onlineshop, Spiele, usw.), die nur Kontaktdaten enthalten, in Ihrer Grafik weg.**

## 4.2. Beispiel
Die Darstellungsart steht Ihnen frei. Zur Inspiration können diese Grafiken dienen:
![Beispiel 1 grafische Darstellung persönliches Ablagekonzept](images/ablagekonzept_beispiel_1.png)

[Beispiel 2 grafische Darstellung persönliches Ablagekonzept](images/ablagekonzept_beispiel_2.png)

# 5. Das eigene Ablagekonzept analysieren
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Daten nach schutzwürdigkeit einstufen |

In der oben dargestellten Grafik wurden die Datenkategorien und Services teilweise mit Symbolen versehen, die ihre Schutzwürdigkeit definieren. Grundsätzlich gilt es eine Unterscheidung zwischen allgemeinen Daten und schutzwürdigen Personendaten zu machen. Zusätzlich machen wir noch eine Unterscheidung zwischen eigenen und fremden Personendaten. Den während wir das Recht über unsere eigenen Personendaten haben, so müssen wir bei Personendaten von Dritten uns an die Vorgaben des Datenschutzgesetzes halten. Wir definieren die folgende Kategorien:
 - Allgemeine Daten
 - Schützenswerte eigene Personendaten
 - Schützenswerte Personendaten von Dritten

Teilen Sie nun alle Ihre Datenablagen und verwendeten Services in diese drei Kategorien ein. 

# 6. Weiterführende Links und Quellen
 - https://medium.com/golden-data/what-is-a-filing-system-under-eu-data-protection-law-6e7222743f71
 - https://www.data.cam.ac.uk/data-management-guide/organising-your-data
 - https://www.asianefficiency.com/organization/organize-your-files-folders-documents/