# Backup <!-- omit in toc -->

<b>Kein Backup - Kein Mitleid</b>

Backups manuell zu erstellen oder ein automatisches Backup zu konfigurieren gehört zu den Tätigkeiten, die keinen unmittelbaren Vorteil bringen: Wenn man die Fotos der letzten Sommerferien vom Laptop auf eine externe Festplatte kopiert und diese anschliessend sicher verstaut, dann bringt das im Moment keinen Benefit. Weil es keinen unmittelbaren Vorteil bringt und aufwändig ist, wird das Backup häufig vernachlässigt. Nicht selten benötigt es ein einschneidendes Erlebnis bis man die Notwendigkeit von Backups einsieht und man sich zuverlässig darum kümmert. Während der Verlust aller Familienfotos, des Gaming-Spielstandes oder einer grösseren Schularbeit bereits sehr schmerzhaft und traumatisierend sein kann, muss bei einem Datenverlust in einer Firma schnell mit einem Sachschaden von mehreren hunderttausend Franken gerechnet werden. 


**Definition Backup:**
```Ein Backup ist die räumliche Trennung der Orginaldatenträger und der Backup Medien.```

In Rahmen dieses Moduls schauen wir uns die Grundlagen von Backups an und fokussieren uns auf das Backup der persönlichen Daten. Wie man ein Backup für einen Server einrichtet und weiterführende Themen erlernen Sie in fortführenden Modulen zu einem späteren Zeitpunkt in ihrer Lehrzeit. 

Für das persönliche Backup dienen folgende Grundsätze:
 - Neuen Laptop erhalten: Als erstes Backup einrichten! (Nicht verschieben! Hohes Risiko das zu Vergessen: Aus einem *später* wird schnell ein *nie*)
 - Neues Handy einrichten: Backup konfigurieren!<br/>(Meistens erfolgt ein automatisches Backup auf Google Drive oder iCloud **Achtung: Datenschutzaspekte bedenken!**)
 - 8 Stunden an einem Dokument gearbeitet, aber die Backup-HDD für das Notebook nicht dabei und lieber keine Cloud verwenden: Alle Dateien mit [7z](https://www.7-zip.org/) Zippen, mit Passwort versehen und auf einen Filehosting-Dienst laden. 
 - Gerade einen Switch am konfigurieren: Nach jeder grösseren Änderung kurz einen Config Export machen: Wenn man die Konfiguration kaputt macht, kann man wieder auf die funktionierende Version zurück. 
 - Firmwareupdate auf einer Firewall: Vorher ein Backup der Konfiguration machen.
 - usw.

Damit Sie sich möglichst schnell daran gewöhnen **immper** Backups zu machen gilt an der TBZ deshalb die Regel "**Kein Backup - Kein Mitleid**". Das bedeutet: Wenn der Laptop, auf dem zum Beispiel die aktuellste Version ihres Portfolios ist, gestohlen wird, Sie ihn verlieren oder er kaputt geht, dann gibt es kein Verständnis oder Nachfristen. Im schlimmsten Fall kann das die Note 1 bedeuten. Unserer konsequenten Haltung erleichtert es Ihnen sich an das Backup machen zu gewöhnen. Am Schluss ihrer Lehrzeit sollte *Backupen* so selbstverständlich sein wie Zähneputzen. Wenn Während der Verlust einer Schularbeit verkraftbar ist, kann der Verlust von wichtigen Geschäftsdaten zu Sach- und Personenschäden führen. 

Denn es gilt *Murphys law: "Anything that can go wrong will go wrong."* ([Wikipedia](https://de.wikipedia.org/wiki/Murphys_Gesetz)). Die Frage ist nicht, **ob**, sondern **wann** Sie das Backup benötigen.

**Erfüllen Sie Ihre Sorgfaltspflicht und erstellen Sie IMMER und AUSNAHMSLOS Backups!**

https://www.storage-insider.de/was-ist-ein-backup-eine-datensicherung-a-621411/

# Inhaltsverzeichnis <!-- omit in toc -->

# 1. Ziele
 - B3G: Kann den Zweck, aber auch die Grenzen eines Backups erläutern.
 - B3F: Plant und macht regelmässig ein Backup seiner Daten.
 - B3E: Setzt ein Backupkonzept ein und überprüft es regelmässig auf Restorefähigkeit.


# 2. Einführung ins Thema Backup
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Leseauftrag |
| Zeitbudget  |  1 Lektion |
| Ziele | Wie wissen was ein Backup ist und die wesentlichen Aspekte |

Informieren Sie sich *Herdt* Buch "Informationstechnologie Grundlagen (Stand 2021)" von Seite 216 bis 223 über die Grundlagen zum Thema Backup.

# 3. Begriffe Klären
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit, 4er Gruppen |
| Aufgabenstellung  | Theoretische Grundkenntnisse erarbeiten |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie kennen den Unterschied zwischen einem Block-Level und File-Level Backup |

Halten Sie die wesentlichen Informationen zu den einzelnen Themen fest und versuchen Sie die Fragen zu beantworten.
Das Ziel ist es, dass sie am Schluss der Klasse das Wesentlichste über das Thema erklären können.

## 3.1. Themen
 - 1. Thema: **Datensicherungsziele, Datenkompression**
   - Welche Medien/Datenträger/Geräte werden als Backup-Ziel verwenden?
   - Was für Kompressionsverfahren werden verwendet?
   - Weshalb ist Datenkompression insbesondere bei einem Image-Backup wichtig?
 - 2. Thema: **Vollsicherung, Differenzielle Sicherung, Inkrementelle Sicherung**
   - Erklären Sie den Unterschied zwischen den drei Methoden mithilfe einer Grafik
   - Wird für die differenzielle und inkrementelle Sicherung ein Vollbackup benötigt?
   - Vor- und Nachteile der verschiedenen Arten.
 - 3. Thema: **Block-Level vs File-Level Backup**
   - Dateisystem-Ebene: Kopieren von einzelnen Dateien vs. Kopieren von Datenblöcken
   - Erklären Sie die Begriffe und die Unterschiede.
   - Welche Methode eignet sich besser, wenn ein Image von einer Festplatte erstellt werden soll und weshalb?
   - Welche Methode eignet sich besser, wenn nur der Home-Folder gesichert werden muss?
 - 4. Thema: **Hot Backup vs Cold Backup**
   - Erklären Sie die Begriffe mithilfe jeweils einen oder zwei praktischen Einsatzzwecken
 - 5. Thema: **Datensicherungsstrategie**
   - Wie häufig soll man ein Backup machen?
   - Auf wie vielen unterschiedlichen Datenträger soll man Backups machen?
   - Begriffe zum Recherchieren: Turm von Hanoi, 3-2-1 Regel

## 3.2. Aufgabenstellung
Ziele: Jede Gruppe recherchiert zu einem der oben aufgeführten Themen und stellt die Erkenntnisse der Klasse vor.

 1. Die Klasse wird in 5 Gruppen aufgeteilt. Jede Gruppe behandelt ein Thema.
 3. 10 Minuten, Einzelarbeit: Recherchieren Sie zum Thema. Benutzen Sie dafür das Internet (Suchmaschine ihrer Wahl) und/oder die unten aufgeführten Links. 
 4. 20 Minuten: Tauschen Sie sich aus. Halten Sie die Erkenntnisse mit Stichworten oder in einer Grafik auf einem Poster fest. 
 5. Je (maximal!) 5 Minuten: Jede Gruppe stellt ihr Poster kurz der Klasse vor. 
 6. Halten Sie zu jedem Begriff die wichtigsten Erkenntnisse im Portfolio fest. 

## 3.3. Quellen
 - https://www.cloudbacko.com/blog/block-level-backup-vs-file-level-backup-which-is-better/
 - https://www.ubackup.com/backup-restore/block-level-backup-4348.html
 - https://de.wikipedia.org/wiki/Datensicherung

# 4. Backupkonzept -  Ich brauche ein Backup - Aber wovon und wie?
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie haben mithilfe des persönlichen Ablagekonzeptes ein einfaches Backupkonzept erstellt |

Im Thema Ablagekonzept haben Sie unter anderem analysiert, an welchen Orten Sie persönliche Daten abspeichern. Nun geht es darum zu identifizieren, bei welchen Daten Sie keinen Verlust in Kauf nehmen möchten und wie Sie für diese Daten eine Datensicherung erstellen können. Die nachfolgenden Arbeitsaufträge sind dabei verschiedene Varianten mit denen Sie von Ihrem Notebook ein Backup erstellen können. Es gibt noch viel mehr Varianten und Lösungen. Die hier beschriebenen Methoden sind lediglich eine Auswahl. Für Ihr Backupkonzept können Sie auch eine andere Methode wählen. 

## 4.1. Aufgabenstellung
 - Erstellen Sie eine Tabelle (z.B. mit Excel oder LibreOffice Calc) mit vier Spalten:
   - **Quelle**: Von was solle eine Datensicherung erstellt werden?
   - (Datensicherungs)**Ziel**: Wohin soll eine Datensicherung erstellt werden?
   - **Häufigkeit**: Wie häufig soll die Datensicherung erstellt werden?
   - **Methode**: Mit welchem Tool (Applikation) wir das Backup erstellt? Wird es automatisch (kontinuierlich oder nach Zeitplan) oder manuell durchgeführt?
 - Füllen Sie mithilfe ihres Ablagekonzeptes die Tabelle aus. 

**Hinweis:** Für Ihr Portfolio müssen Sie das Backup für Ihre *Schuldateien* einrichten. 

# 5. Windows Backup einrichten - Offline
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie haben das Standard Windows Backup auf Ihren Notebook eingerichtet |

"Dateiversionsverlauf" oder "File History" ist das integrierte *file-level based* Backup unter Windows. Es erlaubt Ihnen ohne die Installation von zusätzlicher Software von Ihren persönlichen Dateien eine Datensicherung zu erstellen. 

## 5.1. Aufgabenstellung
Wenn Sie ein Windows Notebook in der Schule verwenden: Nutzen Sie diese Anleitung und richten Sie sich ein Backup ein. https://support.microsoft.com/en-us/windows/file-history-in-windows-5de0e203-ebae-05ab-db85-d5aa0a199255

# 6. Windows Backup einrichten - Online
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie haben OneDrive Backup eingerichtet |

Microsoft möchte möglichst viele Kunden auf Ihre OneDrive Plattform bringen und haben (vermutlich unter anderem) deshalb OneDrive als Backup Methode gleich in Windows 10 integriert. Sie haben ja bereits gelernt, dass die Verwendung von Cloudbasierten Services nicht ohne Risiken sind. 

Informieren Sie sich über das Tool: 
https://support.microsoft.com/de-de/office/aktivieren-von-onedrive-backup-4e44ceab-bcdf-4d17-9ae0-6f00f6080adb

# 7. Cloudbackup - aber sicher
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit, Austausch |
| Aufgabenstellung  | Leseauftrag |
| Zeitbudget  |  20 Minuten |
| Ziele | Sie ein verschlüsseltes Backup auf einem Cloudspeicher eingerichtet. |

Die Cloud ist praktisch und die meisten Accounts kostenlos. Bei Google Drive erhält man man 15 GB (Stand 22.10.2021), bei OneDrive 5 GB, DropBox 2GB. Das reicht für einige Dokumente und auch ein wenig Bilder. Bezahlt man bei DropBox zum Beispiel 120 Euro pro Jahr erhält man 2 TB Speicherplatz. Eine 4 TB USB-Festplatte kostet aktuell ungefähr 80 CHF. Nach bereits einem Jahr, wäre das die günstigere Backup Lösung. Doch ist es auch die bessere?

Vorteile Cloud:
 - Immer übers Internet erreichbar
 - Keine Hardware
 - Über alle gängigen Geräte-Typen erreichbar
 - Weitere Funktionen: Dateien-Teilen, usw. 

Nachteile:
 - Ab einer gewissen Menge Daten: Regelmässige Kosten
 - Datenschutzbedenken
 - Kein physischer Zugang zu den Daten: Abhängig von externer Firma

Deshalb entfielt es sich eine plurale Backupstrategie zu fahren: **Lokale Sicherung + Cloud Sicherung**

Es gibt verschiedene Tools für die Erstellung eines Backups auf einem Cloudspeicher. 

Zwei bekannte OpenSource Tools sind:
 - [Duplicati](https://www.duplicati.com/)
 - [Duplicacy](https://duplicacy.com/)

Hier finden Sie einen Performancenvergleich: https://forum.duplicati.com/t/big-comparison-borg-vs-restic-vs-arq-5-vs-duplicacy-vs-duplicati/9952

## 7.1. Aufgabenstellung
### 7.1.1. Informieren & Planen
 1. Schauen Sie sich die oben aufgeführten Tools an. Kennen Sie noch andere Tools?
 2. Wählen Sie ein Tool aus, dass ihren Anforderungen entspricht. Mögliche Anforderungen: Es unterstützt mein Cloudspeicher (z.B. Google Drive)
 3. Suchen Sie auf der Webseite einer Bedienungsanleitung: Wie richten Sie das gewählte Backup-Tool auf Ihrem PC ein?
 4. Setzen Sie sich ein Ziel (z.B. "Den Ordner *TBZ* auf meinem Laptop in meinem privaten Google Drive Account Sichern") und notieren Sie es sich in Ihrem Portfolio. 
 5. Machen Sie sich einen Plan: Wie gehen Sie vor?

### 7.1.2. Ausführen
Führen Sie die Installation aus und versuchen Sie Ihr Ziel zu erreichen. Wenn Sie auf Probleme stossen, versuchen Sie zuerst es selbst mithilfe des Internets (Suchmaschine) zu lösen. Arbeiten Sie mit einem Kollegen zusammen. Wenn Sie überhaupt nicht mehr weiterkommen, fragen Sie die Lehrperson. 

# 8. Backup prüfen - Jetzt einplanen!
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  15 Minuten |
| Ziele | Sie haben die regelmässige Backup-Überprüfung in Ihrem Terminkalender eingetragen. |

Toll, dass sie jetzt Backups für alle ihre Geräte bzw. Daten eingerichtet haben. Nebst der **regemässigen** Erstellung des Backups kommt ein weiterer ganz wichtiger Punkt dazu: Die **regelmässige** Überprüfung der Backups. Eine genau so traumatische Erfahrung, wie Daten zu verlieren, ist der Moment in dem Sie feststellen: Mein Backup hat gar nie funktioniert. Ein nicht **regelmässig** überprüftes Backup entspricht keinen Backup. **Prüfen Sie regelmässig und pflichtbewusst Ihre Backups!**

Empfohlen: Halbjährliche Überprüfung der Backups

Aufgabenstellung: Tragen Sie in ihrem Kalender einen wiederholenden Termin ein, der Sie daran erinnert das Backup zu überprüfen. 


# Keyfile Backup erstellen

# 9. Nützliche Tools
 - [Clonezilla](https://clonezilla.org/) ist ein eine Open Source Software für die Erstellung vom Images und Klonen von Festplatten. Sie erlaubt es 1:1 Backups von der ganzen Festplatte zu erstellen. 
 - [rsync](https://de.wikipedia.org/wiki/Rsync) Ein Leistungsstarkes Linux Werkzeug zum synchronisieren von Dateien oder Erstellung von Backups
 - [Acronis](https://www.acronis.com/de-de/) Softwarehersteller von leistungsstarker Backup Software
 - [Macrium](https://www.macrium.com/) Softwarehersteller von leistungsstarker Backup Software
 - [File History in Windows](https://support.microsoft.com/en-us/windows/file-history-in-windows-5de0e203-ebae-05ab-db85-d5aa0a199255) Standard Tool zum Erstellen von Dateilevel-Backups
 - [Time Machine (Apple)](https://de.wikipedia.org/wiki/Time_Machine_(Apple)) Dateisicherungssoftware von Apple
 - [Timeshift (Ubuntu)](https://github.com/teejee2008/timeshift) Dateisicherungssoftware für Linux Desktopsysteme
 - [Duplicati](https://www.duplicati.com/) / [Duplicacy](https://duplicacy.com/) Verschlüsseltes Backup in die Cloud
 - 
